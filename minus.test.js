const minus = require('./minus.js');

test('3-2 equal 1', () => {
  const value = minus(3, 2);
  expect(value).toBe(1);
});
